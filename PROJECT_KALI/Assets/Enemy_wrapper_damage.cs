﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_wrapper_damage : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        print("Other.tag: " + other.tag);
        if(other.tag == "Player")
        {
            GameObject.FindGameObjectWithTag("Jackie").GetComponent<Enemy_Basic_1>().DamagePlayer();
        }
    }
}
