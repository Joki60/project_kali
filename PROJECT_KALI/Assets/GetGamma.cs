﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetGamma : MonoBehaviour
{
    Light lt;
    int gamma;
    // Start is called before the first frame update
    void Start()
    {
        lt = this.GetComponent<Light>();
        lt.intensity = PlayerPrefs.GetInt("gamma");
        gamma = PlayerPrefs.GetInt("gamma");
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadPlus) && gamma<= 10)
        {
            gamma = PlayerPrefs.GetInt("gamma") + 1;
            PlayerPrefs.SetInt("gamma", gamma);
            lt.intensity = PlayerPrefs.GetInt("gamma");
        }
        if (Input.GetKeyDown(KeyCode.KeypadMinus) && gamma >= 2)
        {
            gamma = PlayerPrefs.GetInt("gamma")-1;
            PlayerPrefs.SetInt("gamma", gamma);
            lt.intensity = PlayerPrefs.GetInt("gamma");
        }

    }
}
