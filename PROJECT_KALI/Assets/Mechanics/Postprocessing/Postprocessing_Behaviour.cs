﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.PostProcessing;
using UnityEngine;

public class Postprocessing_Behaviour : MonoBehaviour
{
    [SerializeField]
    [Range(0,1)]
    float m_chromaticAberrationIntensity;
    [SerializeField]
    [Range(0,1)]
    float m_vignetteIntensity;
    [SerializeField]
    [Range(0,1)]
    float m_grainIntensity;
    [SerializeField]
    [Range(0,1)]
    float m_motionBlurIntensity;
    PostProcessingBehaviour m_post;

 
    void Start()
    {
        m_post = Camera.main.GetComponent<PostProcessingBehaviour>();
    }

    // Update is called once per frame
    void Update()
    {
        float insanity = 1 - GameObject.Find("Player").GetComponent<Player>().HealthPercentage;
        var ca = m_post.profile.chromaticAberration.settings;
        var v = m_post.profile.vignette.settings;
        var g = m_post.profile.grain.settings;
        var m = m_post.profile.motionBlur.settings;

        ca.intensity = m_chromaticAberrationIntensity * insanity;
        v.intensity = m_vignetteIntensity * insanity;
        g.intensity = m_grainIntensity * insanity;
        m.frameBlending = m_motionBlurIntensity * insanity;

        m_post.profile.chromaticAberration.settings = ca;
        m_post.profile.vignette.settings = v;
        m_post.profile.grain.settings = g;
        m_post.profile.motionBlur.settings = m;
        
    }
}
