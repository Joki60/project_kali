﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp_Oil : MonoBehaviour
{
    public AudioClip oilSound;
    [Range(0,1)]
    [SerializeField]
    float m_oilIncrease;

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.tag == "Player")
        {
             other.GetComponent<Player>().AddOil(m_oilIncrease);
             Destroy(gameObject);
            //SoundManager.instance.PlaySingle(oilSound); 
        }
    }
}
