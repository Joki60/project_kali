﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitch_Haystack : Lightswitch
{
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public override void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "PlayerBullet" && !hasCollided)
        {
            hasCollided = true;
            if(m_lightSounds.Length != 0){
            m_audioSource.clip = m_lightSounds[Random.Range(0,m_lightSounds.Length)];
            m_audioSource.Play();
            }
            ON();
            GetComponentInParent<Animator>().SetTrigger("isBurning");
            Destroy(other.gameObject);
        }   
    }
}
