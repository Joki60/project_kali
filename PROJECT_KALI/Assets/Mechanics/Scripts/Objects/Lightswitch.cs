﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightswitch : MonoBehaviour
{
    protected bool hasCollided = false;
    [SerializeField]
    protected AudioClip[] m_lightSounds;
    protected AudioSource m_audioSource;
    void Awake()
    {
     m_audioSource = this.GetComponent<AudioSource>();   
    }
    public void ON()
    {
        GetComponentInChildren<Lightsource>().Enable();
    }
    public void OFF()
    {
        GetComponentInChildren<Lightsource>().Disable();
    }
    public virtual void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.gameObject.tag == "PlayerBullet")
        {
            if(m_lightSounds.Length != 0){
            m_audioSource.clip = m_lightSounds[Random.Range(0,m_lightSounds.Length)];
            m_audioSource.Play();
            }
            ON();
            Destroy(other.gameObject);
            hasCollided = true;
        }    
    }
}
