﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision_Puddle : Collision
{
    [Header("Puddle Specific")]
    [SerializeField]
    float m_WetTime;
    public override void OnTriggerEnter2D(Collider2D other)
    {
        if(!m_hasCollided && other.gameObject.tag == "Player")
        {
            if (m_hitSounds.Length != 0)
            {
                m_audioSource.clip = m_hitSounds[Random.Range(0, m_hitSounds.Length)];
                m_audioSource.Play();
            }
            other.gameObject.GetComponent<Player>().ModifyHealth(-damagePercentage);
            //This restricts the player movement, doesn't allow player to move up and down, but we want it to so it is now commented out :)
            //other.gameObject.GetComponent<Player>().RestrictMovement(GameObject.FindGameObjectWithTag("ParallaxManager").GetComponent<Background_Parallax_Manager>().CalculateTimeP(m_dec_distance,m_dec_percentage));
            GameObject.FindGameObjectWithTag("ParallaxManager").GetComponent<Background_Parallax_Manager>().Interpolate(m_dec_distance,m_acc_distance,m_dec_percentage,m_waitTime,new Curve(m_acc_easing,m_acc_intensity),new Curve(m_dec_easing,m_dec_intensity),true);
            other.gameObject.GetComponent<Player>().RestrictFire(m_WetTime);
            m_hasCollided = true;


        }
    }
}
