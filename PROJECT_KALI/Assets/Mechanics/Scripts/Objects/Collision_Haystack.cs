﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision_Haystack : Collision
{
    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }
    public void TriggerAnimation(Collider2D other)
    {
        print("Things are happening");
        if (other.gameObject.tag == "Player")
        {
            PlayerCollision(other);
            anim.SetTrigger("isColliding");
            m_hasCollided = true;
        }
    }
    public void OnAnimationComplete()
    {
           Destroy(this.gameObject);
    }
    public override void OnTriggerEnter2D(Collider2D other) 
    {
        if(!m_hasCollided && other.gameObject.tag == "Player")
        {
        TriggerAnimation(other);
        Debug.Log(other.gameObject.tag);
        PlayerCollision(other);
        m_hasCollided = true;
        }
    }
}
