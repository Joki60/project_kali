﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Collision : MonoBehaviour
{
    [SerializeField]
    protected AudioClip[] m_hitSounds;
    protected AudioSource m_audioSource;

    [SerializeField]
    protected float damagePercentage;
    [Header("Knock down")]
    [SerializeField]
    protected float m_dec_distance;
    [SerializeField]
    protected float m_acc_distance;
    [SerializeField]
    protected float m_waitTime;
    [SerializeField]
    protected float m_dec_percentage;
    [Header("Deceleration graph")]
    [SerializeField]
    protected float m_dec_intensity;
    [SerializeField]
    protected EasingFunction m_dec_easing;
    [Header("Acceleration graph")]
    [SerializeField]
    protected float m_acc_intensity;
    [SerializeField]
    protected EasingFunction m_acc_easing;
    protected bool m_hasCollided = false;

    void Awake()
    {
       m_audioSource = this.GetComponent<AudioSource>(); 
    }
    public virtual void OnTriggerEnter2D(Collider2D other) 
    {
        if(!m_hasCollided && other.gameObject.tag == "Player")
        {
        Debug.Log(other.gameObject.tag);
        PlayerCollision(other);
        m_hasCollided = true;
        }
    }
    
    protected void PlayerCollision(Collider2D other)
    {
      if(GameObject.Find("Manager_Scene").GetComponent<Manager_Scene_Main>().m_states != (int)Manager_Scene_Main.States.DEATH || GameObject.Find("Manager_Scene").GetComponent<Manager_Scene_Main>().m_states != (int)Manager_Scene_Main.States.WIN){      
            if(m_hitSounds.Length != 0)
            {
            m_audioSource.clip = m_hitSounds[Random.Range(0,m_hitSounds.Length)];
            m_audioSource.Play();
            }
            other.gameObject.GetComponent<Player>().ModifyHealth(-damagePercentage);
            other.gameObject.GetComponent<Player>().RestrictMovement(GameObject.FindGameObjectWithTag("ParallaxManager").GetComponent<Background_Parallax_Manager>().CalculateTimeP(m_dec_distance,m_dec_percentage));
            GameObject.FindGameObjectWithTag("ParallaxManager").GetComponent<Background_Parallax_Manager>().Interpolate(m_dec_distance,m_acc_distance,m_dec_percentage,m_waitTime,new Curve(m_acc_easing,m_acc_intensity),new Curve(m_dec_easing,m_dec_intensity),true);
            other.gameObject.GetComponent<Player>().RestrictFire(GameObject.FindGameObjectWithTag("ParallaxManager").GetComponent<Background_Parallax_Manager>().CalculateTimeP(m_dec_distance,m_dec_percentage));
            m_hasCollided = true;
      }
    }
}
