﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightsource : Easing
{
    [SerializeField]
    bool ON = false;
    bool Activated = false;
    public enum Shape
    {
        Circle,
        Square
        
    }
    [SerializeField]
    Shape m_shape;
    [SerializeField]
    [Header("Circle")]
    float m_radius;
    [SerializeField]
    [Header("Square")]
    Vector3 m_size;
    [SerializeField]
    Vector3 m_offSet;
    [Header("ON/OFF")]
    [SerializeField]
    float m_startuptime;
    [SerializeField]
    //Lol meeeeeeeeeee
    float m_turnofftime;
    float m_OnOffTimer;
    float m_intensityModifier = 0;
    [SerializeField]
    EasingFunction m_easingFunctionIN;
    [SerializeField]
    float m_intensityIN;
    [SerializeField]
    EasingFunction m_easingFunctionOUT;
    [SerializeField]
    float m_intensityOUT;


    private Light[] m_lights;
    private float[] m_baseIntensity;
    void Awake()
    {
     Transform light = this.transform.GetChild(0);
     light.transform.forward = (new Vector3(light.transform.position.x,light.transform.position.y,0) + m_offSet) - light.transform.position;
    
     if(m_shape == Shape.Square)
     {
         BoxCollider2D bColl = this.gameObject.AddComponent(typeof(BoxCollider2D)) as BoxCollider2D;
         bColl.offset = m_offSet;
         bColl.size = m_size;
         bColl.isTrigger = true;
         bColl.transform.rotation = Quaternion.identity;
     }
     else if(m_shape == Shape.Circle)
     {
        CircleCollider2D cColl = this.gameObject.AddComponent(typeof(CircleCollider2D)) as CircleCollider2D;
        cColl.offset = m_offSet;
        cColl.radius = m_radius;
        cColl.isTrigger = true;
        cColl.transform.rotation = Quaternion.identity;
     }
     #region  Getting the light components
     m_lights = this.GetComponentsInChildren<Light>();
     m_baseIntensity = new float[m_lights.Length];
     for (int i = 0; i < m_lights.Length; i++)
     {
         m_baseIntensity[i] = m_lights[i].intensity;
     }
     #endregion
     m_intensityModifier = (ON)? 1:0;
     foreach (Light l in m_lights)
     {
            l.enabled = true;
            l.intensity *= m_intensityModifier;
     }
    }
    // Update is called once per frame
    void Update()
    {
        if(ON && m_intensityModifier < 1)
        {
           m_intensityModifier = (m_startuptime == 0)? 1: m_intensityModifier + Time.deltaTime/m_startuptime;
           if(m_intensityModifier > 1)
           m_intensityModifier = 1;
           for (int i = 0; i < m_lights.Length; i++)
           {
               switch(m_easingFunctionIN)
               {
                   case EasingFunction.EASECIRCLE: m_lights[i].intensity = m_baseIntensity[i] * EaseInCircle(m_intensityModifier,m_intensityIN);break;
                   case EasingFunction.LINEAR: m_lights[i].intensity = m_baseIntensity[i] * m_intensityModifier;break;
               }
           }
        }
        else if(!ON && m_intensityModifier > 0)
        {
           m_intensityModifier = (m_turnofftime == 0)? 0: m_intensityModifier - Time.deltaTime/m_turnofftime;
           if(m_intensityModifier < 0)
           m_intensityModifier = 0;
           for (int i = 0; i < m_lights.Length; i++)
           {
                switch(m_easingFunctionOUT)
               {
                   case EasingFunction.EASECIRCLE: m_lights[i].intensity = m_baseIntensity[i] * EaseOutCircle(m_intensityModifier,m_intensityIN);break;
                   case EasingFunction.LINEAR: m_lights[i].intensity = m_baseIntensity[i] * m_intensityModifier;break;
               }
           }
        }
    }
   /*private void OnDrawGizmosSelected()
    {
        if(m_shape == Shape.Circle)
        {
        UnityEditor.Handles.color = Color.green;
        UnityEditor.Handles.DrawSolidDisc(this.transform.position + m_offSet,new Vector3(0,0,1),m_radius);
        }
        else if(m_shape == Shape.Square)
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawCube(this.transform.position+ m_offSet, m_size);
        }
        
    }*/
    private void OnTriggerEnter2D(Collider2D other)
    {
        //print("COLLISION");
        if(other.gameObject.tag == "Player" && ON )
        {
            other.gameObject.GetComponent<Player>().m_isInLight = true;
            
        }    
    }
    private void OnTriggerExit2D(Collider2D other) 
    {
        //print("EXITING COLLISION");
        if(other.gameObject.tag == "Player"&& ON)
        {
           other.gameObject.GetComponent<Player>().m_isInLight = false;
        }
    }
    public void Enable()
    {
       ON = true;
    }
    public void Disable()
    {
        ON = false;
    }
}
