﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawnable_Object : MonoBehaviour {

[SerializeField]
Vector3 objectBounderies;

public Vector3 ObjectBounderies{get{return objectBounderies;}}

private void OnDrawGizmosSelected() {
    Gizmos.color =  new Color(1,0,0,0.5f);
	Gizmos.DrawCube(this.transform.position, objectBounderies);
}
}
