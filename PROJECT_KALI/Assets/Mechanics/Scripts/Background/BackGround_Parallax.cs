﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGround_Parallax : MonoBehaviour {
    public enum Direction
	{
      Left,
	  Right		
	}
	[Header("General settings")]
	public Direction m_direction;
	[SerializeField]
	[Tooltip("0 means stationary.The directions are managed through the directions variable")]
	float m_speed;
    [SerializeField]
	[Tooltip("If you wan to spawn objects randomly instead of staticlly (Doesn't render the background if checked)")]
	bool m_generateObjects;
	[Header("Background Settings")]
    [SerializeField]
    GameObject[] m_renderObjects;
	[Tooltip("1 spacing indicates one camerawidth between the items")]
	[SerializeField]
	[Range(0,1)]
	float m_spacing;
	[SerializeField]
	float m_pixelWidth;
	[Header("Object settings")]
	[SerializeField]
	Vector3 m_spawnBounderies;
	[SerializeField]
	Vector3 m_pointOfOrigin;
	[SerializeField]
	[Range(0.2f,2)]
	float m_tickFrequency;
	[SerializeField]
	[Range(0,100)]
	int m_objectSpawnChance;
	Camera m_renderCamera;
	float m_spacingModifier;
	float m_camBND_X,m_camBND_Y;
	[HideInInspector]
	public List<GameObject> m_Objects = new List<GameObject>(); 
	int m_closestToEdge;
	Direction m_lastFrame;
	float m_tickTimer;
	[Header("Gizmos")]
	[SerializeField]
	bool displayLaneWeighting;
	


	void Start () {
		m_renderCamera = Camera.main;
		m_camBND_Y = m_renderCamera.orthographicSize;
		m_camBND_X = m_camBND_Y * (m_renderCamera.aspect);
	    m_spacingModifier = m_camBND_X * 2;
		if(!m_generateObjects)
	    {
		int segmentAmount;
		//Creating the segments 
		if(m_pixelWidth >= m_camBND_X *2)
		{
         		segmentAmount = 3;	
		}
		else
		{
		     segmentAmount = (int)((m_camBND_X*2)/ (m_pixelWidth + (m_spacing*m_spacingModifier /2)));
				 segmentAmount += 2;
		}
		if(segmentAmount%2 == 1)
		{
			//If the amounts of segments are uneven space them accordingly
			int index = 0;
            for(int i = (segmentAmount/2) * -1; i < segmentAmount/2 +1; i++)
			{
				int rand = Random.Range(0, m_renderObjects.Length);
				m_Objects.Add(Instantiate(m_renderObjects[rand],this.transform));
				float x = i * (m_pixelWidth + (m_spacing * (m_spacingModifier / 2)));
				m_Objects[index].transform.position = new Vector2(x,this.transform.position.y);	
				index++;
			}
		}
		else
		{
			//If the amounts of segments are even space them accordingly
			int index = 0;
			for(int i = (segmentAmount/2) * -1; i < segmentAmount/2 + 1; i++)
			{
				if(i == 0){continue;}
				int rand = Random.Range(0, m_renderObjects.Length);
				float x = Mathf.Sign(i)* (((Mathf.Abs(i)-1) * 2) + 1) * (m_pixelWidth/2);
				x += (float)(Mathf.Sign(i) * (Mathf.Abs(i)-0.5) * (m_spacing*m_spacingModifier/2));
				m_Objects.Add(Instantiate(m_renderObjects[rand],this.transform));
				m_Objects[index].transform.position = new Vector2(x,this.transform.position.y);
				index++;
			}	

		}
	  }
	  else
	  {
		//Complete this logic for object generetion
		if(!this.GetComponent<Foreground_ObjectGeneration>()){ print(this.name + "No ForegroundObjectGeneration.cs attached");}
		else
		{
		  float m_distanceTraveledPerTick = (m_tickFrequency/Time.deltaTime) * Time.deltaTime * m_speed * GameObject.FindGameObjectWithTag("ParallaxManager").GetComponent<Background_Parallax_Manager>().m_speedModifier;
		  int m_segmentAmount = (int)(m_spawnBounderies.x / m_distanceTraveledPerTick);
      //m_Objects = GetComponent<Foreground_ObjectGeneration>().InititateObjects(m_segmentAmount,m_pointOfOrigin,m_spawnBounderies);
		  foreach(GameObject go in m_Objects)
		  {
			  go.transform.SetParent(this.transform);
		  }
		}		  
	  }
	  m_closestToEdge = (m_direction == Direction.Left)? 0 : m_Objects.Count-1;
	  m_lastFrame = m_direction;
	  this.transform.parent = GameObject.FindGameObjectWithTag("ParallaxManager").transform;
	}
	
	// Update is called once per frame
	void Update (){
		if(!m_generateObjects)
		{
		if(m_lastFrame != m_direction)
		{
			switch(m_direction)
			{
				case Direction.Left: m_closestToEdge = (m_closestToEdge == m_Objects.Count -1)? 0: m_closestToEdge +1; break;
				case Direction.Right: m_closestToEdge = (m_closestToEdge == 0)? m_Objects.Count -1: m_closestToEdge -1; break;
			}

		}
		switch(m_direction)
		{
			case Direction.Left:
			if(m_Objects[m_closestToEdge].transform.position.x + (m_pixelWidth + (m_spacing*m_spacingModifier/2)) < -m_camBND_X)
			{
			  int temp_i = (m_closestToEdge == 0)? m_Objects.Count-1  : m_closestToEdge - 1;
        float x = m_Objects[temp_i].transform.position.x + (m_pixelWidth + (m_spacing*m_spacingModifier/2));
        m_Objects[m_closestToEdge].transform.position = new Vector3(x,this.transform.position.y,0);
				#region Instantiating new object 
				Transform temp = m_Objects[m_closestToEdge].transform;
				Destroy(m_Objects[m_closestToEdge]);
				m_Objects[m_closestToEdge] = Instantiate(m_renderObjects[Random.Range(0, m_renderObjects.Length)],temp.position,Quaternion.identity);
				m_Objects[m_closestToEdge].transform.parent = this.transform;
				#endregion

				m_closestToEdge = (m_closestToEdge == m_Objects.Count -1)? 0: m_closestToEdge +1;
     
			}break;
			case Direction.Right:
			if(m_Objects[m_closestToEdge].transform.position.x - (m_pixelWidth + (m_spacing*m_spacingModifier/2)) > m_camBND_X)
			{
			  int temp_i = (m_closestToEdge == m_Objects.Count -1)? 0 : m_closestToEdge + 1;
			  float x = m_Objects[temp_i].transform.position.x - (m_pixelWidth + (m_spacing*m_spacingModifier/2));
			  m_Objects[m_closestToEdge].transform.position = new Vector3(x,this.transform.position.y,0);
				#region Instantiating new object
				Transform temp = m_Objects[m_closestToEdge].transform;
				Destroy(m_Objects[m_closestToEdge]);
				m_Objects[m_closestToEdge] = Instantiate(m_renderObjects[Random.Range(0, m_renderObjects.Length)],temp.position,Quaternion.identity);
				m_Objects[m_closestToEdge].transform.parent = this.transform;
				#endregion
		
			  m_closestToEdge = (m_closestToEdge == 0 )? m_Objects.Count -1 : m_closestToEdge -1;

			}break;  			
		}

		}
		else
		{
		 //Spawning the objects
		if(GameObject.Find("Manager_Scene").GetComponent<Manager_Scene_Main>().m_states != (int)Manager_Scene_Main.States.DEATH || GameObject.Find("Manager_Scene").GetComponent<Manager_Scene_Main>().m_states != (int)Manager_Scene_Main.States.WIN){
		 m_tickTimer += Time.deltaTime * GameObject.FindGameObjectWithTag("ParallaxManager").GetComponent<Background_Parallax_Manager>().m_speedModifier;
		 if(m_tickTimer >= m_tickFrequency)
		 {
			 int rand = Random.Range(0,101);
			 if(rand <= m_objectSpawnChance)
			 {
			 GameObject temp = GetComponent<Foreground_ObjectGeneration>().GetRandomObject(m_Objects.ToArray(), m_pointOfOrigin,m_spawnBounderies,m_camBND_X);
			 if(temp != null){
			 temp.transform.SetParent(this.transform);
			 m_Objects.Add(temp);
			 }
			 }
			 m_tickTimer = 0;
		 }
		}
		 //Removing the objects
		 m_Objects.RemoveAll(x => x == null);

            if (m_Objects.Count > 0)
            {
                if (m_Objects[0].transform.position.x + m_Objects[0].GetComponent<Spawnable_Object>().ObjectBounderies.x < -m_camBND_X)
                {
                    Destroy(m_Objects[0]);
                    m_Objects.Remove(m_Objects[0]);
                }
            }

		}
		//Update their position
		foreach(GameObject go in m_Objects)
		{
			Vector3 transformation = new Vector3(m_speed * Time.deltaTime * GameObject.FindGameObjectWithTag("ParallaxManager").GetComponent<Background_Parallax_Manager>().m_speedModifier, 0,0);
			switch(m_direction)
			{
				case Direction.Left: go.transform.position -= transformation; break;
				case Direction.Right: go.transform.position += transformation; break;
			}
		}
		//Removing
		
		m_lastFrame = m_direction;
	}
	private void OnDrawGizmosSelected(){
		if(!displayLaneWeighting)
		{
        Gizmos.color =  new Color(0,0,1,0.5f);
		Gizmos.DrawCube(m_pointOfOrigin,m_spawnBounderies);
		}
		else if(displayLaneWeighting)
		{
			int[] lanes = this.GetComponent<Foreground_ObjectGeneration>().Lanes;
			float totalWeight = 0;
			foreach (int weight in lanes)
			{
				totalWeight += weight;
			}
			for(int i = 0; i < lanes.Length; i++)
			{
			  Gizmos.color = new Color(0,0,1,(float)lanes[i]/totalWeight);
			  Vector3 poo = new Vector3(m_pointOfOrigin.x,m_pointOfOrigin.y - (m_spawnBounderies.y/2) + (m_spawnBounderies.y/lanes.Length/2) + (m_spawnBounderies.y/lanes.Length*i));
			  Vector3 size = new Vector3(m_spawnBounderies.x,m_spawnBounderies.y/lanes.Length);
			  Gizmos.DrawCube(poo,size);
			}
		}
	}
}
