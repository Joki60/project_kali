﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjectWeighting 
{
	[SerializeField]
	public int weight;
	[SerializeField]
	public int m_index;
	[SerializeField]
	public int m_restrictSpawnTime;
	[SerializeField]
	bool m_changeWeight;

	[Header("IF CHANGE WEIGHT")]
	#region ifChangeWeight 
	
		[SerializeField]
		int m_targetWeight;
		[SerializeField]
		int m_changeTime;
		[SerializeField]
		int m_restrictChangeTime;
	#endregion
	    bool increase;
		float m_updateTime;
		float m_updateTimer;

	public void Init()
	{
		increase = (weight - m_targetWeight > 0)? false:true;
		m_updateTime = m_changeTime/Mathf.Abs(weight - m_targetWeight);
	}
	public void UpdateWeight(){
	   
	  if(m_changeWeight && ((increase && weight < m_targetWeight) || (!increase && weight > m_targetWeight)) && m_restrictChangeTime < GameObject.Find("Manager_Scene").GetComponent<Manager_Scene_Main>().ElapsedTime)
	  {
		  m_updateTimer += Time.deltaTime;
		  if(m_updateTimer >  m_updateTime)
		  {
			   m_updateTimer = 0;
			   weight += (increase)? 1: -1;
		  }
	  }	
	}
}
public class Foreground_ObjectGeneration : MonoBehaviour {


	[SerializeField]
	[Tooltip("Have to have the Spawnable_Object Component")]
	GameObject[] m_objects;
	[SerializeField]
	ObjectWeighting[] m_objectWeight;
	[SerializeField]
	[Tooltip("Input the weight of the lane")]
	int[] m_lanes;
	//A lane describes how high a change an object has to spawn in that lane. If you input a total of 4 lanes the avaliable space will be devided into 4 lanes verticalyl
	//[SerializeField]
    //int m_maxAmountOfObjectsPerSpawn;
    public int[] Lanes{get{return m_lanes;}}

	private void Start() 
	{
		foreach(ObjectWeighting ob in m_objectWeight)
		{
             ob.Init();
		}
	}
    private void Update() {
	    foreach(ObjectWeighting ob in m_objectWeight)
		{
             ob.UpdateWeight();
		}
	}
		
	public List<GameObject> InititateObjects(int p_segmentAmount,Vector3 p_pointOfOrigin, Vector3 p_bounderies)
	{
		List<GameObject> returnObjects = new List<GameObject>();
		float m_spacing_x = p_bounderies.x / p_segmentAmount;
        float m_spacing_y = p_bounderies.y / m_lanes.Length;
		bool isOverlapping = false;
		for(int i = 0; i < p_segmentAmount; i++)
		{
			GameObject temp = Instantiate(GetRandomObject(), new Vector3(), Quaternion.identity);
			int lane = GetLane();
		do
		{
			isOverlapping = false;
			float x = (p_pointOfOrigin.x - p_bounderies.x/2) + m_spacing_x * i;
			float y = (lane != 0)? Random.Range((m_spacing_y * (lane -1)) + (p_pointOfOrigin.y - p_bounderies.y/2), (m_spacing_y * lane) + (p_pointOfOrigin.y - p_bounderies.y/2)): Random.Range(p_pointOfOrigin.y - p_bounderies.y/2,p_pointOfOrigin.y + p_bounderies.y/2);
            temp.transform.position = new Vector3(x,y);
			foreach(GameObject go in returnObjects)
			{
			  if(go != null)
              isOverlapping = IsOverlapping(temp,go);
			  else
			  isOverlapping = false;
			  if(isOverlapping){break;}
			}
		}while(isOverlapping);
		returnObjects.Add(temp);
		}
	    return returnObjects;  
	}
	public GameObject GetRandomObject(GameObject[] activeObjetcs ,Vector3 p_pointOfOrigin,Vector3 p_bounderies, float p_x_cameraBND)
	{
		GameObject _temp = GetRandomObject();
		if(_temp == null){return null;}
		GameObject temp = Instantiate(_temp, new Vector3(),Quaternion.identity);
		int lane = GetLane();
		float m_spacing_y = p_bounderies.y / m_lanes.Length;
		bool isOverlapping = false;
		do
		{
         isOverlapping = false;
		 float y = (lane != 0)? Random.Range((m_spacing_y * (lane -1)) + (p_pointOfOrigin.y - p_bounderies.y/2), (m_spacing_y * lane) + (p_pointOfOrigin.y - p_bounderies.y/2)): Random.Range(p_pointOfOrigin.y - p_bounderies.y/2,p_pointOfOrigin.y + p_bounderies.y/2);
		 temp.transform.position = new Vector3(p_x_cameraBND + temp.GetComponent<Spawnable_Object>().ObjectBounderies.x,y);
		 foreach(GameObject go in activeObjetcs)
		 {
           if(go != null)
              isOverlapping = IsOverlapping(temp,go);
			  else
			  isOverlapping = false;
		   if(isOverlapping){break;}
		 }
		}while(isOverlapping);
        return temp;
	}
	private GameObject GetRandomObject()
	{
		float elaspedTime = GameObject.Find("Manager_Scene").GetComponent<Manager_Scene_Main>().ElapsedTime;
		List<ObjectWeighting> accessibleGameObjects = new List<ObjectWeighting>();
		for (int i = 0; i < m_objectWeight.Length; i++)
		{
			if(elaspedTime > m_objectWeight[i].m_restrictSpawnTime)
			{
                 accessibleGameObjects.Add(m_objectWeight[i]);
			}
		}
		if(accessibleGameObjects.Count == 0){print("IM RETURNING NULL"); return null;}
		int m_totalItemWeight = 0;
		foreach(ObjectWeighting go in accessibleGameObjects)
		{
				 m_totalItemWeight += go.weight;
		}
		int rand = Random.Range(1, m_totalItemWeight +1);
		int sumWeight = 0;
		foreach(ObjectWeighting go in accessibleGameObjects)
		{
			sumWeight += go.weight;
			if(sumWeight >= rand){return m_objects[go.m_index];}
		}
		//This will literally never happen lol;
		return null;
	}
	private int GetLane()
	{
		int m_laneWeight = 0;
		foreach(int lane in m_lanes)
		{
			m_laneWeight += lane;
		}
		int rand = Random.Range(1, m_laneWeight +1);
		int sumWeight = 0;
		int index = 1;
		foreach(int lane in m_lanes)
		{
			sumWeight += lane;
			if(sumWeight >= rand){return index;}
			index++;
		}
		return 0;
	}
	bool IsOverlapping(GameObject A, GameObject B)
	{
		 Spawnable_Object A_spawnable = A.GetComponent<Spawnable_Object>();
		 Spawnable_Object B_spawnable = B.GetComponent<Spawnable_Object>();
		 if(!A_spawnable || !B_spawnable){print("ForeGroundObjectGeneration::IsOverlapping::Invalid Object");}
		 float A_Uppercorner_x = A.transform.position.x - A_spawnable.ObjectBounderies.x;
		 float B_Uppercorner_x = B.transform.position.x - B_spawnable.ObjectBounderies.x;
		 float A_Uppercorner_y = A.transform.position.y - A_spawnable.ObjectBounderies.y;
		 float B_Uppercorner_y = B.transform.position.y - B_spawnable.ObjectBounderies.y;
		 float A_Width = A_spawnable.ObjectBounderies.x;
		 float A_Height = A_spawnable.ObjectBounderies.y;
		 float B_Height = B_spawnable.ObjectBounderies.y;
		 float B_Width = B_spawnable.ObjectBounderies.x;
		 bool xOverlap = ValueInRange(A_Uppercorner_x, B_Uppercorner_x, B_Uppercorner_x + B_Width) || ValueInRange(B_Uppercorner_x, A_Uppercorner_x,A_Uppercorner_x +A_Width);
		 bool yOverlap = ValueInRange(A_Uppercorner_y, B_Uppercorner_y, B_Uppercorner_y + B_Height) || ValueInRange(B_Uppercorner_y, A_Uppercorner_y,A_Uppercorner_y +A_Height);
         return (yOverlap && xOverlap);
	}
	bool ValueInRange(float value,float min, float max)
	{
        return (value >= min) && (value <= max);
	}
}
