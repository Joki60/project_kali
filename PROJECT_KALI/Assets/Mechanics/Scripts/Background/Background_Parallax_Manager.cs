﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background_Parallax_Manager : Easing
{
   const int m_maxSpeed = 4;
   public int MaxSpeed{get{return m_maxSpeed;}}
   [Range(0,m_maxSpeed)]
   public float m_speedModifier = 1; 
   private float m_currentSpeed;
   private bool m_activeAcceleration = false;
   private bool m_activeDeceleration = false;
   
   private float m_currentTargetMaxSpeed;
   [Header("Movement")]
   [SerializeField]
   private float m_passiveIncreaseStep;
   [SerializeField]
   private float m_catchupStep;
   [SerializeField]
   [Range(0,100)]
   private float m_speedUpInterval;
   [SerializeField]
   [Tooltip("This should be the same speed as the ground layer")]
   //We will use this to predict how long it will take to slow down / speed up;
   private float m_standardSpeed;
   private float m_speedUpTimer;
   #region current momentum
   float m_time;
   float m_acc_time;
   float m_targetSpeed_dec;
   float m_targetSpeed_acc;
   float m_wait_time;
   float m_acc_percentage;
   float m_target_percentage;
   float m_distance;
   float m_acc_distance;
   float m_dec_target;
   float m_acc_target;
   Curve m_acc_curve;
   Curve m_dec_curve;
   float m_dec_distance;
   float m_dec_percentage;
   //Division ....
   float m_time_elapsed;
   float m_start_speed;
   float m_end_speed;
   EasingFunction m_easing;
   float m_intensity;
   float m_interpolation_startSpeed;
   float m_interpolation_endSpeed;
   #endregion
   int m_state = 0;
   float m_waitTimer = 0;
   bool m_interpolationActive;
   public bool stop = false;
   public bool active = true;
   private void Start()
   {
    m_currentTargetMaxSpeed = m_speedModifier; 
   }
   private void Update() 
   {
       if(active)
       {
        if (m_interpolationActive)
       {
         Interpolate(m_dec_distance,m_acc_distance,m_dec_percentage,m_wait_time,m_acc_curve,m_dec_curve,false);
       }
       else if(m_activeAcceleration)
       {
         //Accelerate(m_acc_time, m_targetSpeed_acc);
       }
       else if(m_activeDeceleration)
       {
         //Decelerate(m_dec_time, m_targetSpeed_dec);
       }
       else if(!stop)
       {
           if(m_speedModifier < m_currentTargetMaxSpeed)
           {
               m_speedModifier += m_catchupStep * Time.deltaTime;
           }
           if(m_speedModifier > m_currentTargetMaxSpeed)
           {
               m_speedModifier = m_currentTargetMaxSpeed;
           }
           if(m_speedUpTimer >= m_speedUpInterval && m_currentTargetMaxSpeed < m_maxSpeed)
           {
 
               m_speedUpTimer = 0;
               m_currentTargetMaxSpeed += m_passiveIncreaseStep;
               if(m_currentSpeed > m_maxSpeed){m_currentSpeed = m_maxSpeed;}
           }

       }
       m_speedUpTimer += Time.deltaTime;
       if(stop && m_speedModifier > 0)
       {
           m_speedModifier = 0;
       }
       if(m_speedModifier < 0){m_speedModifier = 0;}

       }
      
   }
   public float CalculateTimeP(float p_distance,float p_targetPercentage)
   {
     return (p_distance/(m_standardSpeed *((m_speedModifier + (m_speedModifier* p_targetPercentage))/2)));
   }
   public float CalculateTimeS(float p_distance,float p_targetSpeed)
   {
       return (p_distance/(m_standardSpeed *((m_speedModifier + p_targetSpeed)/2)));
   }

   public bool DecelerateD_Percentage(float p_targetPercentage,float p_distance,Curve curve)
   {
       if(p_targetPercentage >= 1){return true;}
       if(!m_activeDeceleration && !m_activeAcceleration)
       {    
            m_time_elapsed = 0;
            m_start_speed = m_speedModifier;
            m_end_speed = m_speedModifier*p_targetPercentage;
            m_time = (p_distance/(m_standardSpeed *((m_start_speed + m_end_speed)/2)));
            m_easing = curve.easing;
            m_intensity = curve.intensity;
            m_distance = p_distance;
            m_target_percentage = p_targetPercentage;
            m_activeDeceleration = true;
       }
       if(m_activeDeceleration && !m_activeAcceleration)
       {
          return Deceleration();
       }
       return false;
   }
   public bool DecelerateD_SetSpeed(float p_targetSpeed,float p_distance,Curve curve)
   {
       if(p_targetSpeed > m_speedModifier){return true;}
       if(!m_activeDeceleration && !m_activeAcceleration)
       {    
            m_time_elapsed = 0;
            m_start_speed = m_speedModifier;
            m_end_speed = p_targetSpeed;
            m_time = (p_distance/(m_standardSpeed *((m_start_speed + m_end_speed)/2)));
            m_easing = curve.easing;
            m_intensity = curve.intensity;
            m_distance = p_distance;
            m_activeDeceleration = true;
       }
       if(m_activeDeceleration && !m_activeAcceleration)
       {
          return Deceleration();
       }
       return false;
   }
   public bool DecelerationT_Percentage(float p_targetPercentage,float p_time,Curve curve)
   {
       if(p_targetPercentage >= 1){return true;}
       if(!m_activeDeceleration && !m_activeAcceleration)
       {  
            m_time_elapsed = 0;
            m_start_speed = m_speedModifier;
            m_end_speed = m_speedModifier * p_targetPercentage;
            m_time = p_time;
            m_easing = curve.easing;
            m_intensity = curve.intensity;
            m_activeDeceleration = true;
       }
       if(m_activeDeceleration && !m_activeAcceleration)
       {
        return Deceleration();
       }
       return false;
   }
   public bool DecelerationT_SetSpeed(float p_targetSpeed,float p_time,Curve curve)
   {
       if(p_targetSpeed > m_speedModifier){return true;}
       if(!m_activeDeceleration && !m_activeAcceleration)
       {    
            m_time_elapsed = 0;
            m_start_speed = m_speedModifier;
            m_end_speed = p_targetSpeed;
            m_time = p_time;
            m_easing = curve.easing;
            m_intensity = curve.intensity;
            m_activeDeceleration = true;
       }
       if(m_activeDeceleration && !m_activeAcceleration)
       {
        return Deceleration();
       }
       return false;
   }
   public bool AccelerateD_Percentage(float p_targetPercentage,float p_distance,Curve curve)
   {
       if(p_targetPercentage <= 1){return true;}
       if(!m_activeAcceleration && !m_activeDeceleration)
       {    
            m_time_elapsed = 0;
            m_start_speed = m_speedModifier;
            m_end_speed = m_speedModifier*p_targetPercentage;
            m_time = (p_distance/(m_standardSpeed *((m_start_speed + m_end_speed)/2)));
            m_easing = curve.easing;
            m_intensity = curve.intensity;
            m_distance = p_distance;
            m_target_percentage = p_targetPercentage;
            m_activeAcceleration = true;
       }
       if(m_activeAcceleration && !m_activeDeceleration)
       {
         return Acceleration();
       }
       return false;
   }
   public bool AccelerateD_SetSpeed(float p_targetSpeed,float p_distance,Curve curve)
   {
       
       //if(p_targetSpeed <= m_speedModifier){print("Wallleeeeeee");return true;}
       if(!m_activeAcceleration && !m_activeDeceleration)
       {    
            m_time_elapsed = 0;
            m_start_speed = m_speedModifier;
            m_end_speed = p_targetSpeed;
            m_time = (p_distance/(m_standardSpeed *((m_start_speed + m_end_speed)/2)));
            m_easing = curve.easing;
            m_intensity = curve.intensity;
            m_distance = p_distance;
            m_activeAcceleration = true;
       }
       if(m_activeAcceleration && !m_activeDeceleration)
       {
          return Acceleration();
       }
       return false;
   }
   public bool AccelerateT_Percentage(float p_targetPercentage,float p_time,Curve curve)
   {
       if(p_targetPercentage <= 1){return true;}
       if(!m_activeAcceleration && !m_activeDeceleration)
       {    
            m_time_elapsed = 0;
            m_start_speed = m_speedModifier;
            m_end_speed = m_speedModifier * m_target_percentage;
            m_time = p_time;
            m_easing = curve.easing;
            m_intensity = curve.intensity;
            m_activeAcceleration = true;
       }
       if(m_activeAcceleration && !m_activeDeceleration)
       {
           return Acceleration();
       }
       return false;
   }
   public bool AccelerateT_SetSpeed(float p_targetSpeed,float p_time,Curve curve)
   {
       if(p_targetSpeed <= m_speedModifier){return true;}
       if(!m_activeAcceleration && !m_activeDeceleration)
       {    
            m_time_elapsed = 0;
            m_start_speed = m_speedModifier;
            m_end_speed = p_targetSpeed;
            m_time = p_time;
            m_easing = curve.easing;
            m_intensity = curve.intensity;
            m_activeAcceleration = true;
       }
       if(m_activeAcceleration && !m_activeDeceleration)
       {
           return Acceleration();
       }
       return false;
   }
   private bool Deceleration()
   {
           
           m_time_elapsed += Time.deltaTime;if(m_time_elapsed > m_time){m_time_elapsed = m_time;}
           switch(m_easing)
           {  
               case EasingFunction.EASECIRCLE:
               m_speedModifier = m_end_speed + ((m_start_speed-m_end_speed)*EaseOutCircle(m_time_elapsed/m_time,m_intensity));
               break;
               case EasingFunction.LINEAR:
               m_speedModifier = m_end_speed + ((m_start_speed-m_end_speed)*LinearOut(m_time_elapsed/m_time));
               break;
           }
           if(m_time_elapsed >= m_time)
           {
               m_time_elapsed = 0;
               m_activeDeceleration = false;
               return true;
           }
           return false;
   }
   private bool Acceleration()
   {
           m_time_elapsed += Time.deltaTime;if(m_time_elapsed > m_time){m_time_elapsed = m_time;}
           switch(m_easing)
           {  
               case EasingFunction.EASECIRCLE:
               m_speedModifier = m_start_speed + ((m_end_speed-m_start_speed)*EaseInCircle(m_time_elapsed/m_time,m_intensity));
               break;
               case EasingFunction.LINEAR:
               m_speedModifier = m_start_speed + ((m_end_speed-m_start_speed)*LinearIn(m_time_elapsed/m_time));
               break;
           }
           if(m_time_elapsed >= m_time)
           {
               m_time_elapsed = 0;
               m_activeAcceleration = false;
               return true;
           } 
           return false;
   }
   public bool Interpolate(float p_dec_distance, float p_acc_distance, float p_dec_percentage,float p_wait_time,Curve p_acc_curve,Curve p_dec_curve, bool reset)
   {
       if(!m_interpolationActive)
       {
           m_dec_distance = p_dec_distance;
           m_acc_distance = p_acc_distance;
           m_dec_percentage = p_dec_percentage;
           m_wait_time = p_wait_time;
           m_acc_curve = p_acc_curve;
           m_dec_curve = p_dec_curve;
           m_start_speed = m_speedModifier;
           m_interpolationActive = true;
       }
       if(m_interpolationActive && reset)
       {
         switch(m_state)
         {
             case 2:
            m_state = 0;
            m_dec_distance = p_dec_distance;
            m_acc_distance = p_acc_distance;
            m_dec_percentage = p_dec_percentage;
            m_wait_time = p_wait_time;
            m_acc_curve = p_acc_curve;
            m_dec_curve = p_dec_curve;
            break;
         }   

       }
       else if(m_interpolationActive)
       {
           //The deceleration thing is fucked.... It's because i'm calculating with constant speed although the speed is dynamic
           switch(m_state)
           {
               case 0:m_state = (DecelerateD_Percentage(m_dec_percentage,m_dec_distance,m_dec_curve) == true)? 1: 0;break;
               case 1:m_waitTimer++; if(m_waitTimer >= m_wait_time){m_state = 2;m_waitTimer = 0;m_interpolation_endSpeed = m_speedModifier;} break;
               case 2:m_state = (AccelerateD_SetSpeed(m_start_speed,m_acc_distance,m_acc_curve)== true)? 0: 2; if(m_state == 0){m_interpolationActive = false;}return true; 
           }
       }
       return false;
   }
}
