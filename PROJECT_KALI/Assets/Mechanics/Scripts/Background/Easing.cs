﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Easing : MonoBehaviour
{
  //intensity nearing 0 results in a steeper drop
  //2 is standard
  public float EaseOutCircle(float value,float intensity)
  {
     return 1- Mathf.Sqrt(1-Mathf.Pow(value - 1,intensity));
  }
  public float EaseInCircle(float value,float intensity)
  {
     return 1- Mathf.Sqrt(1-Mathf.Pow(value,intensity));
  }
  public float LinearOut(float value)
  {
     return 1-value;
  }
  public float LinearIn(float value)
  {
     return value;
  }

}
 public enum EasingFunction
 {
    EASECIRCLE,
    LINEAR
 }
 public struct Curve
 {
   public Curve(EasingFunction p_easing, float p_intensity)
   {
     this.easing = p_easing;
     this.intensity = p_intensity;
   }
   
   public EasingFunction easing;
   public float intensity;

 }
  //intens
