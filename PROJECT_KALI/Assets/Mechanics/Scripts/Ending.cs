﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ending : MonoBehaviour
{
    public float m_fadeSpeed;
    private bool fading;
    private CanvasGroup canvas;

    void Start()
    {
        fading = false;
        canvas = GameObject.FindGameObjectWithTag("FadingEnd").GetComponent<CanvasGroup>();
    }

    void Update()
    {
        if(fading)
        {
            
            if (canvas.alpha < 1)
            {
                canvas.alpha += Time.deltaTime / m_fadeSpeed;
            }
            else if (canvas.alpha >= 1)
            {
                canvas.alpha = 1;
                Cursor.visible = true;
            }
        }
    }
    public void Fade()
    {
        fading = true;   
    }

    public void Transition()
    {
        SceneManager.LoadScene(5);
    }
}
