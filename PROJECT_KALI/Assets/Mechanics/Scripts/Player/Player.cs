﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Player : MonoBehaviour {

    enum WinningStates
	{
		TRANSITIONTOCAMPOSITION,
		TRANSITIONTOENDSCENE,
		TRANSITIONTOHOUSE,
	}
	#region Variables
    [SerializeField]
    [Header("General settings")]
	float m_maxHealth;
	float m_currentHealth;
	[SerializeField]
	[Range(0.01f,2)]
	float m_healthTickFrequency;
	[SerializeField]
	float m_healthIncrease;
	[SerializeField]
	float m_healthDecrease;
	float m_healthTickTimer;
	[SerializeField]
	int m_speed;
	public bool m_isInLight = false;
    [SerializeField]
	[Header("Collider")]
	Vector3 m_offset;
	[SerializeField]
	Vector3 m_size;
	[SerializeField]
	LayerMask m_collideable;
	[Header("Shooting")]
	[SerializeField]
	string m_bulletName;
	[SerializeField]
	int m_magazineSize;
	[SerializeField]
	float m_reloadTime;
	[SerializeField]
	[Tooltip("In seconds")]
	float m_fireRate;
	float m_fireRateTimer;
	float m_movementRestrictTimer;
	float m_fireRestrictTimer;
	float m_movementRestrictTime;
	float m_fireRestrictTime;
	bool m_restricFire;
	bool m_restrictMovement;
	float m_reloadTimer;
	int m_currentMagazineLoad;
	float m_reloadTimePerBullet;
	bool m_reload;
	float m_reloadCooldownTimer;
	bool m_reloadCooldown = false;
	[Header("Lantern")]
	[Range(0,1)]
	[SerializeField]
	float m_OilStartAmount;
	[Tooltip("Per second")]
	[Range(0,1)]
	[SerializeField]
	float m_oilDrain;
    float m_oil;
    bool m_lanternActive = false;
	Lantern m_lantern;
	[SerializeField]
	KeyCode m_PositiveButton;
	[SerializeField]
	KeyCode m_AlternativePositiveButton;
	public bool IsWet{get{return m_restricFire;}}
	public int Bullets{get{return m_currentMagazineLoad;}}
	public float HealthPercentage{get{return m_currentHealth/m_maxHealth;}}
	public float OilPercentage{get{return m_oil;}}
	[HideInInspector]
	public bool m_IsWinning = false;
	private Vector3 m_snaptocamposition_transition;
	WinningStates m_winStates = WinningStates.TRANSITIONTOCAMPOSITION;
	//AUDIO
	AudioSource m_audioSource;
	[SerializeField]
	AudioClip[] m_stepSounds;
	//ANIMATOR
	Animator m_animator;
	[SerializeField]
	[Range(1,2)]
	float m_runningAnimationSpeed_max;
	float m_runningAnimationSpeed;
	
	#endregion

	// Use this for initialization
	void Start () {
		m_oil = m_OilStartAmount;
		m_currentHealth = m_maxHealth;
		m_fireRateTimer = m_fireRate;
		m_currentMagazineLoad = m_magazineSize;
		m_reloadTimePerBullet = m_reloadTime/m_magazineSize;
		m_snaptocamposition_transition = new Vector3(Camera.main.transform.position.x,GameObject.Find("Parallax_Midground_1").transform.position.y);
		m_lantern = gameObject.GetComponentInChildren<Lantern>();
		m_animator = GetComponent<Animator>();
		m_audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update (){
		if(!m_IsWinning){
		#region Update timers
		m_fireRateTimer+= Time.deltaTime;
		m_healthTickTimer+= Time.deltaTime;
		if(m_restricFire)
		m_fireRestrictTimer += Time.deltaTime;
		if(m_restrictMovement)
		m_movementRestrictTimer += Time.deltaTime;
		if(m_reload)
		m_reloadTimer += Time.deltaTime;
		if(m_reloadCooldown)
		m_reloadCooldownTimer += Time.deltaTime;
		if(m_lanternActive){m_oil -= m_oilDrain * Time.deltaTime; m_oil = (m_oil < 0)? 0: m_oil;}
		#endregion
		#region Checking timers
		if(m_fireRestrictTimer >= m_fireRestrictTime)
		{
		   m_fireRestrictTimer = 0;
		   m_restricFire = false;
		}
		if(m_movementRestrictTimer >= m_movementRestrictTime)
		{
		   m_movementRestrictTimer = 0;
		   m_restrictMovement = false;
		}
		if(m_reloadCooldownTimer > m_reloadTimePerBullet){m_reloadCooldownTimer = 0; m_reloadCooldown = false;}
	
		#endregion
		#region Health
		if(m_healthTickTimer > m_healthTickFrequency)
		{
		    if(m_isInLight && m_currentHealth < m_maxHealth){m_currentHealth += m_healthIncrease;}
			else if(m_lanternActive && !m_isInLight && m_currentHealth < m_maxHealth){m_currentHealth += m_healthIncrease;}
			else if(!m_isInLight && m_currentHealth > 0){m_currentHealth -= m_healthDecrease;}
			m_currentHealth = (m_currentHealth > m_maxHealth)? m_maxHealth:(m_currentHealth <0)? 0: m_currentHealth;
			m_healthTickTimer = 0;
		}
		
		#endregion
		#region Reload
		if((m_currentMagazineLoad == 0 || (Input.GetAxis("Reload") > 0 && !m_reloadCooldown)) && !m_reload && m_currentMagazineLoad != m_magazineSize){m_reload = true;m_reloadCooldown = true;}
		if(m_reload && m_reloadTimer >= m_reloadTimePerBullet)
		{
			m_currentMagazineLoad++;
			m_reloadTimer = 0;
			if(m_currentMagazineLoad == m_magazineSize+1)
			{
				m_currentMagazineLoad = m_magazineSize;
				m_reload = false;
			}
		}
		#endregion
        #region Fire
		if(Input.GetAxisRaw("Fire1") > 0 && m_fireRateTimer > m_fireRate && !m_restricFire && m_currentMagazineLoad > 0)
		{
			//FIX CURSOR TARGETING
			Shoot();
			m_fireRateTimer = 0;
			m_currentMagazineLoad--;
			m_reloadTimer = 0;
		}
		#endregion
		#region Movement
        float y = 0;
		if((Input.GetAxisRaw("Vertical") >  0 && !CheckCollision(transform.up)) || (Input.GetAxisRaw("Vertical") < 0 && !CheckCollision(-Vector3.up)))
		{
           y = Input.GetAxisRaw("Vertical") * m_speed * Time.deltaTime;
		}
	    if(!m_restrictMovement)
		transform.Translate(new Vector3(0,y,0));
		#endregion
		#region  Lantern
		if(!m_lanternActive && (Input.GetKeyDown(m_PositiveButton) || Input.GetKeyDown(m_AlternativePositiveButton)) && m_oil > 0)
		{
			m_lanternActive = true;
			m_lantern.SetActive(true);
		}
		else if(m_lanternActive && (Input.GetKeyDown(m_PositiveButton) || Input.GetKeyDown(m_AlternativePositiveButton)))
		{
			print("Lantern inactive");
			m_lanternActive = false;
			m_lantern.SetActive(false);
		}
		if(m_oil == 0)
		{
			m_lanternActive = false;
			m_lantern.SetActive(false);
		}
		#endregion
		#region  Animation
		float speedModifier = 1 + ((GameObject.Find("ParallaxManager").GetComponent<Background_Parallax_Manager>().m_speedModifier-1)/(GameObject.Find("ParallaxManager").GetComponent<Background_Parallax_Manager>().MaxSpeed -1) * (m_runningAnimationSpeed_max-1));
		m_animator.SetFloat("Speed", speedModifier); 
		#endregion
		}
		
		else if(m_IsWinning)
		{
			print("Is winning");
			//BAD CODE REDO LATER
			m_currentHealth = (m_currentHealth < m_maxHealth)? m_currentHealth + (Time.deltaTime*20): m_maxHealth;
			switch(m_winStates)
			{
				case WinningStates.TRANSITIONTOCAMPOSITION:
				if((m_snaptocamposition_transition - transform.position).normalized.x  < 0){m_winStates = WinningStates.TRANSITIONTOENDSCENE;}
				transform.position += (m_snaptocamposition_transition - transform.position).normalized * 200 * Time.deltaTime;
				break;
				case WinningStates.TRANSITIONTOENDSCENE:
				if((new Vector3(GameObject.FindGameObjectWithTag("EndScene").transform.position.x, transform.position.y) - transform.position).normalized.x  < 0){m_winStates = WinningStates.TRANSITIONTOHOUSE;}
				transform.position += (new Vector3(GameObject.FindGameObjectWithTag("EndScene").transform.position.x, transform.position.y)  - transform.position).normalized * 200 * Time.deltaTime;
				Camera.main.transform.position = new Vector3(transform.position.x,Camera.main.transform.position.y,Camera.main.transform.position.z);
				break;
				case WinningStates.TRANSITIONTOHOUSE:
				break;
			}
		}
	}
	//Draws the virtual collider (Doesn't actually do anything) just pretty shapes :)
	private void OnDrawGizmosSelected() {
		Gizmos.color =  new Color(1,0,0,0.5f);
		Gizmos.DrawCube(this.transform.position+m_offset, m_size);
	}
	//Modifies the health based on percentage;
	public void ModifyHealth(float p_healthModifier)
	{
	   if(!m_IsWinning)
	   {
		m_currentHealth += m_maxHealth * p_healthModifier;
	   	m_currentHealth = (m_currentHealth > m_maxHealth)? m_maxHealth:(m_currentHealth <0)? 0: m_currentHealth;
	   }
	}
	public void AddOil(float p_percentage)
	{
		p_percentage = Mathf.Abs(p_percentage);
		m_oil = (m_oil + p_percentage > 1)? 1: m_oil + p_percentage;
	}
	//Uses raycasts (Laser beams :)) to check collision. If the raycast interferes with a collider (With the right tag) then it returns true; else false duh... (Which means that it collided)
	bool CheckCollision(Vector3 p_direction)
	{
	  float length = Mathf.Sqrt(Mathf.Pow(m_size.x * p_direction.x/2,2) + Mathf.Pow(m_size.y * p_direction.y/2,2));
	  Debug.DrawRay(transform.position + m_offset, p_direction * length);
	  return Physics2D.Raycast(this.transform.position + m_offset, p_direction,length,m_collideable);

    }
	void OnTriggerEnter2D(Collider2D other)
	{
		if(GameObject.Find("Manager_Scene").GetComponent<Manager_Scene_Main>().m_states != (int)Manager_Scene_Main.States.DEATH || GameObject.Find("Manager_Scene").GetComponent<Manager_Scene_Main>().m_states != (int)Manager_Scene_Main.States.WIN){
		switch(other.gameObject.tag)
		{
			case "Haystack":
            m_animator.SetTrigger("isCollidingWithHaystack");
			break;
			case "Puddle":
            m_animator.SetTrigger("isCollidingWithPuddle");
			break;
			case "Wagon":
            m_animator.SetTrigger("isCollidingWithWagon");
			break;
			case "Jackie_Collider":
			m_animator.SetTrigger("isCollidingWithShadowhand");
			break;
			default:
			break;
		}
		}	
	}
	public void RandomFootstep()
	{
		if(m_stepSounds.Length > 0)
		{
          m_audioSource.clip = m_stepSounds[Random.Range(0,m_stepSounds.Length)];
          m_audioSource.Play();
		}
	}
	//Loads the bullet from resources and point in the direction that the firepoint is pointing. Why i don't just set the direction on the bullet directly is a mystery......
	void Shoot()
   	{
	   GameObject.Find("FirePoint").transform.right = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,Camera.main.ScreenToWorldPoint(Input.mousePosition).y,this.transform.position.z) - GameObject.Find("FirePoint").transform.position;
       GameObject temp = (GameObject)Instantiate(Resources.Load(m_bulletName),GameObject.Find("FirePoint").transform.position, Quaternion.identity);
	   temp.transform.right = GameObject.Find("FirePoint").transform.right;
	}
	public void DeathAnimation()
	{
		m_animator.SetTrigger("isDying");
	}
	//Restricts movement lol. Basically if m_restrictMovement is not true then you can move. This method just resets the timer for the movement restriction, the rest is handled in the update
	public void RestrictMovement(float time)
	{
        m_movementRestrictTime = time;
		m_movementRestrictTimer = 0;
		m_restrictMovement = true;
	}
	public void RestrictFire(float time)
	{
		m_fireRestrictTime = time;
		m_fireRestrictTimer = 0;
		m_restricFire = true;
	}
}
