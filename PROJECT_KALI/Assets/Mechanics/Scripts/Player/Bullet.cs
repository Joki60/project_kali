﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
	[SerializeField]
    AudioClip[] m_matchesSound;

    [SerializeField]
	float m_bulletspeed;
	[SerializeField]
	int m_hitMask;
	// Use this for initialization
	void Start (){
		this.GetComponent<AudioSource>().clip = m_matchesSound[Random.Range(0, m_matchesSound.Length)];
        this.GetComponent<AudioSource>().Play();
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += m_bulletspeed * Time.deltaTime * transform.right;
		float y_bound = Camera.main.orthographicSize;
        float x_bound = y_bound * (Camera.main.aspect);
		if(transform.position.x > x_bound || transform.position.x < -x_bound || transform.position.y > y_bound || transform.position.y < -y_bound)
		{
			Destroy(this.gameObject);
		}
	}
}
