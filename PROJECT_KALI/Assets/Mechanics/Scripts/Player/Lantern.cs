﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lantern : Easing
{
    //Goes from 0-1
    float m_intensityModifier;
    bool m_active;
    [SerializeField]
    float m_startIntensity;
    [SerializeField]
    float m_maxIntensity;
    [SerializeField]
    float m_interPolationTime;
    Light[] m_lights; 
    
    void Start()
    {
       m_lights = GetComponentsInChildren<Light>();
    }
    // Update is called once per frame
    void Update()
    {
        m_intensityModifier += (m_active && m_intensityModifier < 1)? Time.deltaTime/m_interPolationTime:(!m_active && m_intensityModifier > 0)? -(Time.deltaTime/m_interPolationTime): 0;
        m_intensityModifier = (m_intensityModifier > 1)? 1:(m_intensityModifier < 0)? 0: m_intensityModifier;
        if((m_active && m_intensityModifier < 1) ||(!m_active && m_intensityModifier > 0))
        {
            foreach(Light light in m_lights)
            {
                light.intensity = m_startIntensity + ((m_maxIntensity-m_startIntensity)* EaseInCircle(m_intensityModifier,2));
            }
        }
    }
    public void SetActive(bool p_active)
    {
        m_active = p_active;
    }
}
