﻿using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;

public class Manager_Scene_Main : MonoBehaviour
{
     public enum States
    {
        DEFAULT,
        DEATH,
        WIN
    }
    [Header("Lose")]
    [SerializeField]
    float m_fadeSpeed;
    [Header("Win")]
    [SerializeField]
    EasingFunction m_decelerate_easing;
    [SerializeField]
    float m_decelerate_easing_intensity;
    [SerializeField]
    float m_decelerate_time;
    [Header("Win end scene")]
    [SerializeField]
    GameObject m_endScene;
    [SerializeField]
    float m_endScenePixelWidth;
    [Header("General")]
    [SerializeField]
    float m_time_win;
    public int m_states = 0;
    bool m_freezeStateUpdate = false;
    float m_time_elapsed;
    public float ElapsedTime{get{return m_time_elapsed;}}

    void Start()
    {
        m_states = (int)States.DEFAULT;
        print(m_decelerate_easing);
    }
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(1);
        }
        #region Updating timers
        m_time_elapsed += Time.deltaTime;
        #endregion
        #region Updating states
        if(!m_freezeStateUpdate && GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().HealthPercentage == 0)
        {
            m_states = (int)States.DEATH;
            m_freezeStateUpdate = true;
            GameObject.FindGameObjectWithTag("ParallaxManager").GetComponent<Background_Parallax_Manager>().stop = true;
            GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().RestrictFire(Mathf.Infinity);
            GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().RestrictMovement(Mathf.Infinity);
            GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().DeathAnimation();
            Lightswitch[] lights = GameObject.FindGameObjectWithTag("ParallaxManager").GetComponentsInChildren<Lightswitch>();
            foreach (Lightswitch l in lights)
            {
                l.OFF();
            }
        }
        if(!m_freezeStateUpdate && m_time_elapsed >= m_time_win)
        {
            m_states = (int)States.WIN;
        }
        #endregion
        switch(m_states)
        {
         case (int)States.DEATH:
            CanvasGroup canvas = GameObject.FindGameObjectWithTag("Fading").GetComponent<CanvasGroup>();
            if (canvas.alpha < 1)
            {
                canvas.alpha += Time.deltaTime / m_fadeSpeed;
            }
            else if(canvas.alpha >= 1)
            {
                canvas.alpha = 1;
                Cursor.visible = true;
            }
         break;
         case (int)States.WIN:
            if(!m_freezeStateUpdate && GameObject.FindGameObjectWithTag("ParallaxManager").GetComponent<Background_Parallax_Manager>().DecelerationT_SetSpeed(1,m_decelerate_time,new Curve(m_decelerate_easing,m_decelerate_easing_intensity)) == true)
            {
                m_freezeStateUpdate = true;
                Instantiate(m_endScene,new Vector3((Camera.main.orthographicSize * Camera.main.aspect)+m_endScenePixelWidth,0,0),Quaternion.identity);
                GameObject.FindGameObjectWithTag("ParallaxManager").GetComponent<Background_Parallax_Manager>().m_speedModifier = 0;
                GameObject.FindGameObjectWithTag("ParallaxManager").GetComponent<Background_Parallax_Manager>().active = false;
                GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().RestrictFire(1f/0f);
                GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().RestrictMovement(1f/0f);
                GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().m_IsWinning = true;
            }
         break;
        }
    }
}
