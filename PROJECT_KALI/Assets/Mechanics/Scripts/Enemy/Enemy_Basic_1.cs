﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Basic_1 : Easing
{
    [SerializeField]
    AudioClip[] m_gigglingSound;
    [SerializeField]
    AudioClip[] m_attackSound;
    [SerializeField]
    AudioClip[] m_hitSound;
    [SerializeField]
    AudioClip[] m_evadeSound;
    AudioSource m_audioSource;


    public enum Stages
    {
       POSITION,
       WAIT,
       MOVEINTOSCREEN,
       MOVEOUTOFSCREEN,
       ATTACK,
       MOVEINTOSCREENAFTERATTACK,
       IDLE
    }
    [Header("Lane")]
    [SerializeField]
    int[] m_lanes;
    [SerializeField]
    int m_laneSpacing;
    [Header("Movement")]
    Vector3 m_startPoint;
    [SerializeField]
    Vector3 m_endPoint_LocalPosition;
    Vector3 m_endPoint;
    //Remove when game launch
    bool isRunning = false;
    Stages m_stage;
    Vector3 m_pointOfOrigin;
    [Header("Wait")]
    float m_waitTime;
    [SerializeField]
    Vector2 m_waitRange;
    [SerializeField]
    float m_restrictTime;
    float m_restrictTimer;
    [Header("Move")]
    [SerializeField]
    float m_interpolateTimeIn;
    [SerializeField]
    float m_interpolateTimeOut;

    float m_interpolateTimeElapsed;
    [SerializeField]
    EasingFunction m_intoEasing;
    [SerializeField]
    float m_intoIntesity;
    [SerializeField]
    EasingFunction m_outEasing;
    [SerializeField]
    float m_outIntensity;
    [Header("Idle")]
    [SerializeField]
    float m_idleTime;
    float m_idleTimer;
    [Header("Attack")]
    [SerializeField]
    [Range(0,1)]
    float m_damage;
    [SerializeField]
    float m_exitAttackTime;
    float m_exitAttackTimer;
    #region timers
    float m_waitTimer = 0;
    
    #endregion
    Animator m_animator;
    bool m_ableToHit = true;
    

    void Start()
    {
        m_animator = gameObject.GetComponentInChildren<Animator>();
        m_startPoint = transform.position;
        isRunning = true;
        m_pointOfOrigin = transform.position;
        m_audioSource = this.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
    if(m_restrictTimer < m_restrictTime)
    m_restrictTimer += Time.deltaTime;

    if(m_restrictTimer >= m_restrictTime)
    {
        switch(m_stage)
        {
            case Stages.POSITION:
            int lane = Random.Range(0,m_lanes.Length);
            //Change the point of origion when you know the size
            m_startPoint = new Vector3(m_pointOfOrigin.x,m_pointOfOrigin.y - (m_laneSpacing/2) + (m_laneSpacing/m_lanes.Length/2) + (m_laneSpacing/m_lanes.Length*lane));
            m_endPoint = new Vector3(-(Camera.main.orthographicSize * Camera.main.aspect),m_startPoint.y + m_endPoint_LocalPosition.y);
            m_stage = Stages.WAIT;
            break;

            case Stages.WAIT:
            if(m_waitTimer == 0){m_waitTime = Random.Range(m_waitRange.x,m_waitRange.y +1);}
            m_waitTimer += Time.deltaTime;
            if(m_waitTimer > m_waitTime)
            {
                m_waitTimer = 0;
                m_stage = Stages.MOVEINTOSCREEN;
                transform.position = m_startPoint;
                m_animator.SetTrigger("isEntering");
                //SOUND
                if(m_gigglingSound.Length != 0){
                m_audioSource.clip = m_gigglingSound[Random.Range(0,m_gigglingSound.Length)];
                m_audioSource.Play();
                }
            }
            break;

            case Stages.MOVEINTOSCREEN:
            m_interpolateTimeElapsed += Time.deltaTime;
            if(m_interpolateTimeElapsed > m_interpolateTimeIn){m_interpolateTimeElapsed = m_interpolateTimeIn;}
            Vector3 deltaPosition = m_endPoint - m_startPoint;
                switch(m_intoEasing)
                {
                    case EasingFunction.EASECIRCLE:
                    transform.position = new Vector3(
                        m_startPoint.x + (EaseInCircle(m_interpolateTimeElapsed / m_interpolateTimeIn,m_intoIntesity)* deltaPosition.x),
                        m_startPoint.y + (EaseInCircle(m_interpolateTimeElapsed / m_interpolateTimeIn, m_intoIntesity)* deltaPosition.y));
                    break;
                    case EasingFunction.LINEAR:
                    transform.position = new Vector3(
                        m_startPoint.x + (LinearIn(m_interpolateTimeElapsed / m_interpolateTimeIn)* deltaPosition.x),
                        m_startPoint.y + (LinearIn(m_interpolateTimeElapsed / m_interpolateTimeIn)* deltaPosition.y));
                    break;
                }
            if(m_interpolateTimeElapsed >= m_interpolateTimeIn){m_interpolateTimeElapsed = 0; m_stage = Stages.IDLE;}
            
            break;

            case Stages.IDLE:

            m_idleTimer += Time.deltaTime;
            if(m_idleTimer > m_idleTime){m_idleTimer = 0; m_stage = Stages.ATTACK; m_animator.SetTrigger("isExiting");}
            break;
            
            case Stages.ATTACK:
            m_exitAttackTimer += Time.deltaTime;
            if(m_exitAttackTimer > m_exitAttackTime){m_exitAttackTimer = 0; m_animator.SetTrigger("isAttacking");}
            break;

            case Stages.MOVEOUTOFSCREEN:
            print("Move out of screen");
            m_interpolateTimeElapsed += Time.deltaTime;
            if(m_interpolateTimeElapsed > m_interpolateTimeOut){m_interpolateTimeElapsed = m_interpolateTimeOut;}
            Vector3 _deltaPosition = m_endPoint - m_startPoint;

            switch(m_outEasing)
            {
                case EasingFunction.EASECIRCLE:
                transform.position = new Vector3(
                    m_startPoint.x + (EaseOutCircle(m_interpolateTimeElapsed/m_interpolateTimeOut,m_outIntensity)*_deltaPosition.x),
                    m_startPoint.y + (EaseOutCircle(m_interpolateTimeElapsed/m_interpolateTimeOut,m_outIntensity)*_deltaPosition.y));
                break;
                case EasingFunction.LINEAR:
                transform.position = new Vector3(
                    m_startPoint.x + (LinearOut(m_interpolateTimeElapsed/m_interpolateTimeOut)*_deltaPosition.x),
                    m_startPoint.y + (LinearOut(m_interpolateTimeElapsed/m_interpolateTimeOut)*_deltaPosition.y));
                break;
            }
            if(m_interpolateTimeElapsed >= m_interpolateTimeOut){m_interpolateTimeElapsed = 0; m_stage = Stages.POSITION;}
            break;
        }
    }
    }
    public void AttackDone()
    {
       if(m_stage == Stages.ATTACK)
       {
           m_animator.SetTrigger("isEntering");
           m_stage = Stages.MOVEINTOSCREEN;
           m_ableToHit = true;
       }
    }
    public void Swoosh()
    {
        if(m_attackSound.Length != 0){
        m_audioSource.clip = m_attackSound[Random.Range(0,m_attackSound.Length)];
        m_audioSource.Play();
        }
    }
    //Remove when build 
    private void OnDrawGizmosSelected() 
    {
        if(!isRunning){
        m_startPoint = transform.position;
        m_pointOfOrigin = this.transform.position;
        }
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(m_startPoint, 10);
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(new Vector3(-(Camera.main.orthographicSize * Camera.main.aspect),m_startPoint.y + m_endPoint_LocalPosition.y),10);    
        	
			float totalWeight = 0;
			foreach (int weight in m_lanes)
			{
				totalWeight += weight;
			}
			for(int i = 0; i < m_lanes.Length; i++)
			{
			  Gizmos.color = new Color((float)(i/10.0f),(float)(i/10.0f),(float)(i/10.0f),(float)m_lanes[i]/totalWeight);
			  Vector3 poo = new Vector3(m_pointOfOrigin.x,m_pointOfOrigin.y - (m_laneSpacing/2) + (m_laneSpacing/m_lanes.Length/2) + (m_laneSpacing/m_lanes.Length*i));
			  Vector3 size = new Vector3(100,m_laneSpacing/m_lanes.Length);
			  Gizmos.DrawCube(poo,size);
			}  
    }
    public void DamagePlayer()
    {
        print("Damaging player");
        if(m_ableToHit)
        {
           if(m_hitSound.Length != 0){
           m_audioSource.clip = m_hitSound[Random.Range(0,m_hitSound.Length)];
           m_audioSource.Play();
           }
           GameObject.Find("Player").GetComponent<Player>().ModifyHealth(-m_damage);
           m_ableToHit = false;
        }
    }
    public void ExitScreen()
    {
         m_animator.SetTrigger("isRetreating");
         m_stage = Stages.MOVEOUTOFSCREEN;
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "PlayerBullet" && (m_stage == Stages.ATTACK || m_stage == Stages.IDLE))
        {
            if(m_evadeSound.Length != 0){
            m_audioSource.clip = m_evadeSound[Random.Range(0,m_evadeSound.Length)];
            m_audioSource.Play();
            }
            ExitScreen();
            Reset();
        }
    }
    private void Reset() {
        m_idleTimer = 0;
        m_waitTimer = 0;
        m_exitAttackTimer = 0;
        m_interpolateTimeElapsed = 0;
    }
}
