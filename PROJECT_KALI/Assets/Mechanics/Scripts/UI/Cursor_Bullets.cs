﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Cursor_Bullets : MonoBehaviour
{
    private Text bullet_count;
    [SerializeField]
    Color m_Color_Inactive;
    [SerializeField]
    Color m_Color_Active;
    [SerializeField]
    Vector3 m_offset;
    Player m_player;

    void Start()
    {
        bullet_count = this.GetComponentInChildren<Text>();
        m_player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        
        bullet_count.text = m_player.Bullets.ToString();
        if(m_player.IsWet && this.GetComponent<Image>().color != m_Color_Inactive)
        {
            this.GetComponent<Image>().color = m_Color_Inactive;
        }
        else if(!m_player.IsWet && this.GetComponent<Image>().color != m_Color_Active)
        {
            this.GetComponent<Image>().color = m_Color_Active;
        }
        if(bullet_count.color != this.GetComponent<Image>().color){bullet_count.color = this.GetComponent<Image>().color;}
        Vector3 screenPoint = Input.mousePosition;
        screenPoint.z = 10.0f;
        transform.position = Camera.main.ScreenToWorldPoint(screenPoint) - m_offset;
        this.transform.position = new Vector3(transform.position.x,transform.position.y,transform.position.z);
    }
}
