﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class IntroScene : MonoBehaviour
{
    
    void Start()
    {
        VideoPlayer vidplay = GameObject.FindObjectOfType<VideoPlayer>().GetComponent<VideoPlayer>();
        vidplay.Play();

        vidplay.loopPointReached += NextScene;
    }
    
    void Update()
    {
        VideoPlayer vidplay = GameObject.FindObjectOfType<VideoPlayer>().GetComponent<VideoPlayer>();

        if (Input.anyKey)
        {
            NextScene(vidplay);
        }
    }

    void NextScene (VideoPlayer vid)
    {
        GameObject.Find("VideoPlayer").SetActive(false);

        SceneManager.LoadScene(3);

    }
}
