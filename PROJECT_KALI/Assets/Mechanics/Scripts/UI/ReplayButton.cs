﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReplayButton : MonoBehaviour
{
    public void OnPress ()
    {
        if ((int)Manager_Scene_Main.States.DEATH == GameObject.Find("Manager_Scene").GetComponent<Manager_Scene_Main>().m_states)
        {
            SceneManager.LoadScene(4);
        }
    }
}
