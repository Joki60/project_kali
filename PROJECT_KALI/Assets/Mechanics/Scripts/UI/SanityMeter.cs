﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SanityMeter : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
         this.GetComponent<Slider>().value = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().HealthPercentage;
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Slider>().value = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().HealthPercentage;
    }
}
