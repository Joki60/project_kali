﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OilMeter : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GameObject player = GameObject.Find("Player");
        this.transform.localScale = new Vector3(1, player.GetComponent<Player>().OilPercentage, 1);
    }

    public void UpdateMeter()
    {
        
    }
}
