﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Sanity_Meter : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<SpriteMask>().alphaCutoff = 1 - (GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().HealthPercentage);
    }
}
