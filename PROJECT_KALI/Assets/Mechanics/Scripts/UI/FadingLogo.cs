﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FadingLogo : MonoBehaviour
{
    [SerializeField]
    public float duration;
    float start;
    float end;
    float current;
    int m_fadeStates = 0;

    void Start()
    { 
        start = Time.time;
        end = start + duration;
        current = 0f;
        GameObject.FindGameObjectWithTag("Fading").GetComponent<CanvasGroup>().alpha = 0;
        print("HAY");
    }
    
    void Update()
    {
        current = Time.time - start;
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene(1);
        }
        switch(m_fadeStates)
        {
            case 0:
            GameObject.FindGameObjectWithTag("Fading").GetComponent<CanvasGroup>().alpha = (1 / (duration / current));
            if(GameObject.FindGameObjectWithTag("Fading").GetComponent<CanvasGroup>().alpha >= 1)
            {
                GameObject.FindGameObjectWithTag("Fading").GetComponent<CanvasGroup>().alpha = 1;
                m_fadeStates = 1;
                start = Time.time;
            }
            break;
            case 1:
            GameObject.FindGameObjectWithTag("Fading").GetComponent<CanvasGroup>().alpha = 1 - (1 / (duration / current));
            if(GameObject.FindGameObjectWithTag("Fading").GetComponent<CanvasGroup>().alpha <= 0)
            {
                GameObject.FindGameObjectWithTag("Fading").GetComponent<CanvasGroup>().alpha = 0;
                SceneManager.LoadScene(1);
            }
                       
            break;
        }
    }
}
