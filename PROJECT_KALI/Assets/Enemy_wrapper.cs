﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_wrapper : MonoBehaviour
{
   public void AttackDone()
   {
       gameObject.GetComponentInParent<Enemy_Basic_1>().AttackDone();
   }
   public void Swoosh()
   {
       gameObject.GetComponentInParent<Enemy_Basic_1>().Swoosh();
   }
}
