﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Temporary function for controlling Haystack collision with player and match
public class TempHaystack : MonoBehaviour
{
    Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            anim.SetTrigger("isColliding");
        }
        if (other.gameObject.tag == "PlayerBullet")
        {
            anim.SetTrigger("isBurning");
        }
    }
    public void OnAnimationComplete()
    {
           Destroy(this.gameObject);
    }
}
