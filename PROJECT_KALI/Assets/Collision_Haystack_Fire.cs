﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision_Haystack_Fire : Collision_Haystack
{
     
    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }
    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            PlayerCollision(other);
            anim.SetTrigger("isColliding");
            m_hasCollided = true;
        }
        if (other.gameObject.tag == "PlayerBullet")
        {
            anim.SetTrigger("isBurning");
        }
    }
    public void OnAnimationComplete()
    {
           Destroy(this.gameObject);
    }
}
