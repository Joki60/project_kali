﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GammaController : MonoBehaviour
{
    Light lt;
    int gamma;

    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetInt("gamma", 8);
        gamma = PlayerPrefs.GetInt("gamma");
        /*
        lt = GetComponent<Light>();
        if(PlayerPrefabs.HasKey("gamma"))
        {
            lt.intensity = PlayerPrefabs.GetInt("gamma");
        }
        else
        {
            lt.intensity = 8;
        }*/

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadPlus) && gamma<=10)
        {
            gamma = PlayerPrefs.GetInt("gamma") + 1;
            PlayerPrefs.SetInt("gamma", gamma);
            /*lt.intensity++;
            PlayerPrefabs.SetInt("gamma", lt.intensity);*/
        }
        if (Input.GetKeyDown(KeyCode.KeypadMinus) && gamma >= 2)
        {
            gamma = PlayerPrefs.GetInt("gamma")-1;
            PlayerPrefs.SetInt("gamma", gamma);

            /*lt.intensity--;
            PlayerPrefabs.SetInt("gamma", lt.intensity);*/
        }
    }
}
